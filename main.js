const { app, BrowserWindow } = require('electron')
const path = require('path')

let debug = false

//debug = true

function createWindow () {

    if (debug) {
        windowWidth = 1100
        windowHeight = 700
        windowFrame = true
    } else {
        windowWidth = 700
        windowHeight = 660
        windowFrame = false
    }

    const win = new BrowserWindow({
        width: windowWidth,
        height: windowHeight,
        transparent: true,
        frame: windowFrame,
        resizable: windowFrame,
        titleBarStyle: "hidden", // add this line
        webPreferences: {
            enableRemoteModule: true,
            nodeIntegration: true
        }
    })

    if (debug) {win.webContents.openDevTools()}

    win.loadFile('index.html')
    win.setTitle("Little Helper")
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})
