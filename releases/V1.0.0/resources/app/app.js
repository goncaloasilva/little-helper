const { shell } = require('electron')
const { dialog, app } = require('electron').remote
const store = require('store')
const fs = require('fs')

let _app
let selectedEntries = []
let currentAppntmntId = -1 //Current appointment id

let delete_entry_button = document.getElementById("delete_apntms")
let export_button = document.getElementById("export_apntms")
let add_entry_button = document.getElementById("add_entry")
let ui_list = document.getElementById("_list")
let ui_title = document.getElementById("page_title")
let ui_scroll = document.getElementById("scroll-area")
let ui_desc = document.getElementById("desc")
delete_entry_button.style.display = "none"
export_button.style.display = "none"

class App {
    constructor() {
      //Access memory
      this.db = store.get('user_data')
      //We don't have any data, create a new db
      if(!this.db) {
        this.db = {"app_origin": "little", "appointments": {}}
      }
      this.appointments = this.db["appointments"]
      this.count = Object.keys(this.appointments).length
    }

    saveData() {
      this.db["appointments"] = this.appointments
      store.set('user_data', this.db)
    }

    overwriteDB(new_db) {
      this.appointments = new_db["appointments"]
      this.saveData()
    }
  
    removeAppointment(index) {
      this.count--
      delete this.appointments[index]
    }

    removeLink(appointment_id, index) {
        this.appointments[appointment_id]["links_count"] = this.appointments[appointment_id]["links_count"] - 1
        delete this.appointments[appointment_id]["links"][index]
    }
  
    addAppointment(name, desc, links) {
      this.count++
      this.appointments[this.count] = {"name": name, "desc": desc, "links_count": 0, "links": links}
    }

    addLink(appointment_id, name, link) {
      this.appointments[appointment_id]["links_count"] = this.appointments[appointment_id]["links_count"] + 1
      this.appointments[appointment_id]["links"][this.appointments[appointment_id]["links_count"]] = {"name": name, "link": link}
    }

    exportAppointments(appointments_ids, path) {
      let export_dict = {"app_origin": "little", "appointments": {}}
      let i = 1
      appointments_ids.forEach((id) => {
        export_dict["appointments"][i] = this.appointments[id]
        i++
      })
      fs.writeFileSync(path, JSON.stringify(export_dict))
      UIkit.notification({message: 'Exported '+appointments_ids.length+' appointments <span uk-icon="icon: check">', status: 'success', pos: 'bottom-center', timeout: 1500})
    }

    get aappointments() {
      this.saveData()
      return this.appointments
    }
}

function _select(id) {
  let type
  if(selectedEntries.includes(id)) {
    //Remove from selection array
    selectedEntries.splice(selectedEntries.indexOf(id), 1);
  } else {
    //Add to selection array
    selectedEntries.push(id)
  }

  if(currentAppntmntId == -1) {
    type = "Appointments"
  } else {
    type = "Links"
  }

  if(selectedEntries.length != 0) {
    delete_entry_button.style.display = "block"
    if (currentAppntmntId == -1) {
      export_button.style.display = "block"
    }
    delete_entry_button.innerHTML = "Delete "+selectedEntries.length+" "+ type
    export_button.innerHTML = "Export "+selectedEntries.length+" Appointments"
  } else {
    delete_entry_button.style.display = "none"
    export_button.style.display = "none"
  }
}

function refresh_entries_list(data, isAppointment) {
  ui_list.innerHTML = ""
  if (Object.keys(data).length > 4) {
    ui_scroll.style.height = "228px"
  } else {
    ui_scroll.style.height = "fit-content"
  }
  Object.entries(data).forEach(([id, object]) => {
    if (isAppointment) {
      ui_list.innerHTML += '<tr><td><input onClick="_select('+id+')" class="uk-checkbox" type="checkbox"></td><td class="uk-table-link"><a class="uk-link-reset" onClick="goToAppointment('+id+')" href="#">'+object["name"]+'</a></td></tr>'
    } else {
      ui_list.innerHTML += '<tr><td><input onClick="_select('+id+')" class="uk-checkbox" type="checkbox"></td><td class="uk-table-link"><a class="uk-link-reset" onClick="shell.openExternal(\''+object["link"]+'\')" href="#">'+object["name"]+'</a></td></tr>'
    }
  });
}

function add_appointment() {
  let name, desc
  Swal.fire({
    title: ' ',
    input: 'text',
    showCancelButton: true,
    inputPlaceholder: 'Appointment name',
    showClass: {
      popup: 'animate__animated animate__fadeIn'
    },
    hideClass: {
      popup: 'animate__animated animate__fadeOut'
    }
  }).then((result) => {
    if(result.isConfirmed) {
      if (result.value == "") {
        name = "My new appointment #" + eval(Object.keys(_app.aappointments).length + 1)
      } else {
        name = result.value
      }
      //I shouldn't chain promises like this
      Swal.fire({
        title: ' ',
        input: 'text',
        showCancelButton: true,
        inputPlaceholder: 'Appointment description',
        showClass: {
          popup: 'animate__animated animate__fadeIn'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOut'
        }
      }).then((result) => {
        if(result.isConfirmed) {
          if (result.value == "") {
            desc = "My new appointment" 
          } else {
            desc = result.value
          }
          _app.addAppointment(name, desc, {})
          refresh_entries_list(_app.aappointments, true)
          UIkit.notification({message: 'Added appointment <span uk-icon="icon: check">', status: 'success', pos: 'bottom-center', timeout: 1500})
        }
      })
    }
  })
}

function add_link() {
  let name
  Swal.fire({
    title: ' ',
    input: 'text',
    showCancelButton: true,
    inputPlaceholder: 'Link name',
    showClass: {
      popup: 'animate__animated animate__fadeIn'
    },
    hideClass: {
      popup: 'animate__animated animate__fadeOut'
    }
  }).then((result) => {
    if(result.isConfirmed) {
      if (result.value == "") {
        name = "My new Link #" + eval(_app.aappointments[currentAppntmntId]["links_count"] + 1)
      } else {
        name = result.value
      }
      //I shouldn't chain promises like this  #AGAIN
      // Also, there's a better way of doing this, I'm just being lazy
      Swal.fire({
        title: ' ',
        input: 'url',
        showCancelButton: true,
        inputPlaceholder: 'Link description',
        inputValidator: (value) => {
          if (!value) {
            UIkit.notification({message: 'Please provide a valid url', status: 'danger', pos: 'bottom-center', timeout: 221500})
            return " "
          }
        },
        showClass: {
          popup: 'animate__animated animate__fadeIn'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOut'
        }
      }).then((result) => {
        if(result.isConfirmed) {
          _app.addLink(currentAppntmntId, name, result.value)
          refresh_entries_list(_app.aappointments[currentAppntmntId]["links"], false)
          UIkit.notification({message: 'Added Link <span uk-icon="icon: link">', status: 'success', pos: 'bottom-center', timeout: 1500})
        }
      })
    }
  })
}

function delete_entries() {
  if (currentAppntmntId == -1) {
    selectedEntries.forEach(key => {
      _app.removeAppointment(key)
      refresh_entries_list(_app.aappointments, true)
    })
  } else {
    selectedEntries.forEach(key => {
      _app.removeLink(currentAppntmntId, key)
    })
    refresh_entries_list(_app.aappointments[currentAppntmntId]["links"], false)
  }
  selectedEntries = []
  delete_entry_button.style.display = "none"
}

function export_entries() {
  const options = {
    defaultPath: app.getPath('documents'),
    filters: [
      { name: 'Little Helper Appointment File', extensions: ['json']}
    ]
  }
  dialog.showSaveDialog(null, options).then((path) => {
    if(path && path != "") {
      _app.exportAppointments(selectedEntries, path.filePath)
    }
  });
}

function goToAppointment(id) {
  let current_appointment = _app.aappointments[id]
  ui_title.innerHTML = "<strong><span onClick='home()' style='cursor:pointer' uk-icon='icon: chevron-left; ratio: 2'></span> My Appointments / " + current_appointment["name"] + "</strong>"
  add_entry_button.innerHTML = "<span style='vertical-align: text-top;'' uk-icon='icon: link'></span> New Link"
  add_entry_button.onclick = add_link
  ui_desc.innerHTML = "<strong>Description: </strong>" + current_appointment["desc"]
  refresh_entries_list(current_appointment["links"], false)
  currentAppntmntId = id;
  selectedEntries = []
}

function home() {
  ui_title.innerHTML = "<strong><span uk-icon='icon: calendar; ratio: 2'></span> My Appointments </strong>"
  add_entry_button.innerHTML = "<span style='vertical-align: text-top;'' uk-icon='icon: plus'></span> New Appointment"
  add_entry_button.onclick = add_appointment
  ui_desc.innerHTML = ""
  refresh_entries_list(_app.aappointments, true)
  currentAppntmntId = -1;
  selectedEntries = []
}

document.addEventListener('drop', (event) => { 
  event.preventDefault(); 
  event.stopPropagation(); 

  for (const f of event.dataTransfer.files) { 
      if(f.type == "application/json") {
        fs.readFile(f.path, 'utf-8', (err, data) => {
          if(err){
              alert("An error ocurred reading the file :" + err.message);
              return;
          }
          load_dict = JSON.parse(data)
          if(Object.keys(load_dict).includes("app_origin")) {
            _app.overwriteDB(load_dict)
            home()
          } else {
            return;
          }
        });
      }
    } 
}); 

document.addEventListener('dragover', (e) => { 
  e.preventDefault(); 
  e.stopPropagation(); 
}); 

_app = new App()
home()