const customTitlebar = require('custom-electron-titlebar')
const Swal = require('sweetalert2')
const remote = require('electron').remote

const menu = new remote.Menu();
menu.append(new remote.MenuItem({
	label: 'About',
	click: () => {
        Swal.fire({
            title: 'Little Helper 1.0.0',
            html: 'Made with Electron JS by Gonçalo Abreu Corrêa Brito da Silva.' +
                  '<br><br>Libraries used:'+
                  '<ul>'+
                  '<li>custom-electron-titlebar</li>'+
                  '<li>sweetalert2</li>'+
                  '<li>fs</li>'+
                  '<li>Store.js</li>'+
                  '<li>Animate.css</li>'+
                  '<li>UIkit</li>'+
                  '</ul>',
            showClass: {
              popup: 'animate__animated animate__fadeIn popup-modifier'
            },
            customClass: {
                content: 'custom-text'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOut'
            }
          })
    }
}));

window.addEventListener('DOMContentLoaded', () => {
    let titlebar = new customTitlebar.Titlebar();
    
    titlebar.updateBackground(customTitlebar.Color.fromHex('#444'))
    titlebar.updateMenu(menu);
    titlebar.updateTitle(" ")
})