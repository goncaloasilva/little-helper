# Little Helper 
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)

> A small program that lets you store links in an organized way

### 🏠 [Homepage](https://gitlab.com/goncaloasilva/little-helper)

## Install

```sh
git clone https://gitlab.com/goncaloasilva/little-helper && cd little-helper && npm install
```

## Usage

```sh
npm start
```

## Author

👤 **Gonçalo Abreu Corrêa Brito Da Silva**

* Gitlab: [@goncaloasilva](https://github.com/goncaloasilva)
